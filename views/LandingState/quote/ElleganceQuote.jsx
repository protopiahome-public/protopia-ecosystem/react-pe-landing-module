import React from "react"
import { getStyle } from "../Section"
import Style from "style-it"
import { getFontNameByID } from "../data/PalettePresets"
import { ReactComponent as QuoteStart } from '../../../assets/img/quote-start.svg';
import { ReactComponent as QuoteFinish } from '../../../assets/img/quote-finish.svg'; 
import { ReactComponent as Quote1 } from '../../../assets/img/quotes1.svg'; 
import { ReactComponent as Quote2 } from '../../../assets/img/quotes2.svg'; 
import { ReactComponent as CommaElleganceStart } from '../../../assets/img/comma-ellegance_finish.svg'; 
import { ReactComponent as CommaElleganceFinish } from '../../../assets/img/comma-ellegance_start.svg'; 

const ElleganceQuote = props =>
{
    const { palette, revertColor, data } = props
    const {
        class_name, 
        style, 
        quotes_size, quote_design,
        text, text_order, text_css,
        name, name_order, name_css,
        description, description_order, description_css,
        thumbnail, thumbnail_order, thumbnail_css,
        is_top_quotation_mark,
        is_bottom_quotation_mark,
        quotation_mark_position,
        quote_line_design
    } = data
    const media = thumbnail
        ?
        <div 
        className="landing-quote-thumbnail" 
        style={{
            order: thumbnail_order,
            ...getStyle(thumbnail_css)
        }}
        />
        :
        null
    const getStartQuote = () =>
    {
        if(!is_top_quotation_mark) return null
        switch(quote_design)
        {
            case "elegance_hooks":
                return <div className="quotes in" style={{ transform: "rotate(180deg) translateY(23%)" }}>
                    <Quote1 width={quotes_size } height={quotes_size * 2} />
                </div>
            case "conservative_hooks":
                return <div className="quotes in" style={{ transform: "rotate(180deg) translateY(23%)" }}>
                    <Quote2 width={quotes_size / 2} height={quotes_size } />
                </div>
            case "ellegance_comma":
                return <div className="quotes in" >
                    <CommaElleganceStart width={quotes_size } height={quotes_size } />
                </div> 
            case "comma":
            default:
                return <div className="quotes in" >
                    <QuoteStart width={quotes_size} height={quotes_size}/>
                </div>
        }        
    }
    const getFinishQuote = () =>
    {
        if(!is_bottom_quotation_mark) return null
        switch(quote_design)
        {
            case "elegance_hooks":
                return <div className="quotes out" >
                    <Quote1 width={quotes_size } height={quotes_size * 2} />
                </div>
            case "conservative_hooks":
                return <div className="quotes out" >
                    <Quote2 width={quotes_size /2 } height={quotes_size } />
                </div>
            case "ellegance_comma":
                return <div className="quotes out" >
                    <CommaElleganceFinish width={ quotes_size } height={ quotes_size } />
                </div> 
            case "comma":
            default:
                return <div className="quotes out" >
                    <QuoteFinish  width={quotes_size} height={quotes_size}/>
                </div>
        }        
    }
    return Style.it( `
        .landing-quote-thumbnail 
        {
            background-image:url( ${ thumbnail } ); 
        } 
        .quotes
        {
            padding:15px;
            position:relative;
        }
        .quotes svg path, 
        .quotes svg rect 
        {
            fill: ${revertColor};
        }
        .quotes:before
        {
            content: "";
            position: absolute;
            top: calc(50% - ${quotes_size * .1}px);
            left: 0; 
            border-top: ${quotes_size * .2}px ${quote_line_design}  ${revertColor};
            width: calc(50% - ${quotes_size * 1.0}px); 
        }
        .quotes:after
        {
            content: "";
            position: absolute;
            top: calc(50% - ${quotes_size * .1}px);
            right: 0; 
            width: calc(50% - ${quotes_size * 1.0}px);
            border-top: ${quotes_size * .2}px ${quote_line_design}  ${revertColor}; 
        }
        .landing-quote > .text 
        {
            font-family: ${getFontNameByID( palette.card.title.fontFamilyID )};
            font-size: ${palette.card.title.fontSize};
            order: ${text_order};
            display:flex;
            flex-direction: ${
              quotation_mark_position === "horizontal" 
                ?
                `row`
                :
                `column`
            };   
            justify-content: center;         
            align-items: center;         
        }        
        .landing-quote > .text > .quotes > svg path
        {
            
        }
        `,
        <div 
            className={`landing-quote ellegance  landing-section-core ${class_name}`}
            style={{
                color: palette.main_text_color,
                ...style
            }}
        > 
            { media }
            <div 
                className="text" 
                style={{ 
                
                    ...getStyle( text_css )
                }}          
            > 
                {getStartQuote()}
                <span dangerouslySetInnerHTML={{ __html: text }} />
                {getFinishQuote()}  
            </div>
            <div 
                className="name"
                style={{
                    order: name_order,
                    ...getStyle( name_css )
                }}
            >
                <span className="name">
                    { name }
                </span>
            </div>
            <div 
            className="description"
            style={{
                order: description_order,
                ...getStyle( description_css )
            }}
            >
            <span>
                { description }
            </span>
            </div>
        </div>
    )
}

export default ElleganceQuote