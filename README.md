# react-pe-landing-module

 [Модуль](https://protopiahome-public.gitlab.io/protopia-ecosystem/cra-template-pe//modules/) для [ProtopiaEcosystem React Client](https://protopiahome-public.gitlab.io/protopia-ecosystem/cra-template-pe/). Включает в себя набор Экранов, Виджетов, настроек и ассетов для генерации посадочных страниц инструментами WYSIWYG.

## Подробное описание: 

Модуль позволяет собирать и верстать посадочные страницы методом простой сборки ("Взял-перетянул-настроил").

Удобные инструменты тематических контентных модулей и их глубокой кастомизации позволят красиво, удобно и быстро собрать контент по всем правилам современного графического дизайна и в соответствии с требованиями современной веб-разработки.

Качество результата не будет зависить от Вашего уровня владения инструментарием html-вёрстки, интуитивный инструментарий поможет Вам собрать самые оригинальные и строгие дизайны. Но чем выше Ваши навыки, тем более глубокие и тонкие инструменты станут Вам боступны.

[см.](https://protopiahome-public.gitlab.io/protopia-ecosystem/cra-template-pe//landing-module/)

## Установка

1. В корневой папке приложения запускаем

``` 
npm run add-module https://gitlab.com/genagl/react-pe-landing-module
```

 2. Запускаем локальную сессию:

``` 
npm start
```

 или компиллируем приложение для последующего размещения на сервере:

``` 
npm run build
```

3. Переходим на страницу /landing-example


 ## Структура и сценарии использования:

 [см.](https://protopiahome-public.gitlab.io/protopia-ecosystem/cra-template-pe/landing/sections/)